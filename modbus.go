package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/goburrow/modbus"
)

func main() {
	// // Modbus TCP
	client := modbus.TCPClient("10.73.60.120:20256")
	// Read input register 9
	// for r := 99; r < 115; r++ {
	// 	results, err := client.ReadInputRegisters(uint16(r), 2)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Printf("Result register %v  ----> %v\n", r, results)

	// read speed 109
	// read length coil 113

	// }

	// second
	handler := modbus.NewTCPClientHandler("10.73.60.120:20256")
	handler.Timeout = 10 * time.Second
	handler.SlaveId = 0x65
	handler.Logger = log.New(os.Stdout, "test: ", log.LstdFlags)
	// Connect manually so that multiple requests are handled in one connection session
	err := handler.Connect()
	if err != nil {
		fmt.Println(err)
	}
	defer handler.Close()

	client = modbus.NewClient(handler)
	fmt.Println("Starting...")

	results, err := client.WriteMultipleRegisters(702, 1, []byte{0, 3})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("StandBY %v  ----> %v\n", 702, results)
	time.Sleep(3 * time.Second)

	results, err = client.WriteMultipleRegisters(712, 1, []byte{0, 20})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("StandBY %v  ----> %v\n", 712, results)
	time.Sleep(3 * time.Second)

	results, err = client.WriteMultipleRegisters(702, 1, []byte{0, 5})
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("StandBY %v  ----> %v\n", 712, results)
	time.Sleep(3 * time.Second)

	for i := 0; i < 10; i++ {
		results, err = client.ReadInputRegisters(uint16(711), 1)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Printf("StandBY %v  ----> %v\n", 711, results)
		time.Sleep(2 * time.Second)
	}
	// for i := 100; i <= 600; i += 100 {
	// 	results, err := client.ReadInputRegisters(uint16(i), 1)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Printf("Result register %v  ----> %v\n", i, results)
	// }

	// for i := 101; i <= 601; i += 100 {
	// 	results, err := client.WriteMultipleRegisters(202, 2, []byte{0, 100})
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Printf("Result write to register 202  ----> %v\n", results)
	// }

	// for i := 202; i <= 602; i += 100 {
	// 	results, err := client.WriteMultipleRegisters(202, 2, []byte{0, 1})
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Printf("Result write to register 202  ----> %v\n", results)
	// }

	// for {
	// 	for i := 100; i <= 600; i += 100 {
	// 		results, err := client.ReadInputRegisters(uint16(i), 1)
	// 		if err != nil {
	// 			fmt.Println(err)
	// 		}
	// 		fmt.Printf("Info %v  ----> %v\n", i, results)
	// 	}
	// 	time.Sleep(10 * time.Second)
	// }
	// results, err := client.ReadDiscreteInputs(uint16(99), 3)
	// fmt.Printf("Result register99  ----> %v\n", results)

	// results, err := client.ReadDiscreteInputs(uint16(99), 3)
	// fmt.Printf("Result register99  ----> %v\n", results)
	// results, err = client.ReadDiscreteInputs(uint16(100), 3)
	// fmt.Printf("Result register100  ----> %v\n", results)

	// results, err = client.ReadDiscreteInputs(uint16(101), 16)
	// fmt.Printf("Result register101  ----> %v\n", results)
	// results, err = client.ReadDiscreteInputs(uint16(202), 16)
	// fmt.Printf("Result register202  ----> %v\n", results)

	// results, err = client.ReadDiscreteInputs(uint16(105), 1)
	// fmt.Printf("Result register105  ----> %v\n", results)
	// results, err = client.ReadDiscreteInputs(uint16(106), 1)
	// fmt.Printf("Result register106  ----> %v\n", results)

	//third

	// client = modbus.NewClient(handler)
	// for r := 98; r < 110; r++ {
	// 	results, err := client.ReadDiscreteInputs(uint16(r), 3)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	fmt.Printf("Result register %v  ----> %v\n", r, results)

	// }

	// results, err := client.ReadDiscreteInputs(15, 3)
	// // results, err = client.WriteMultipleRegisters(1, 2, []byte{0, 3, 0, 4})
	// // results, err = client.WriteMultipleCoils(5, 10, []byte{4, 3})
	// fmt.Printf("Result1 ----> %v\n", results)

}
